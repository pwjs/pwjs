#!/usr/bin/env python3

import numpy as np

z = int(input("Podaj rozmiar macierzy: "))


matrix1 = np.random.choice([x for x in range(1,5,1)], z*z)
matrix1.resize(z,z)

print ("Macierz")
print (matrix1)
print ("Wyznacznik macierzy")
print (np.linalg.det(matrix1))
